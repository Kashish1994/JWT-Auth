package com.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String getUsers() {
		return "hi";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public void login() {
		System.out.println("Logged In");
	}

	@RequestMapping(value = "/loyalty", method = RequestMethod.GET)
	public void getLoyalty() {
		System.out.println("Got Loyalty Access");
	}
}
